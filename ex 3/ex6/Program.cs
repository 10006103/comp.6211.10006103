﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Radius: ");

            double radius = double.Parse(Console.ReadLine());

            double perimeter = ((2 * Math.PI) * radius);

            Console.WriteLine("Perimeter = {0:F2}", perimeter);
        }
    }
}
